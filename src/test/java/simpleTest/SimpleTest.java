package simpleTest;

import demo.FxRates;
import static org.junit.Assert.*;
import org.junit.Test;

public class SimpleTest
{
    private FxRates fx;
    
    @Test
    public void testFx () {
        String csvFile = "User/Administrator/Downloads/rates.csv";
        
        String c1 = "USD";
        String c2 = "CAD";
        
        fx.csv2Dict (csvFile);
        Double test_rate = fx.getFXQoute (c2, c1);
        
        assertEquals(1.0012, test_rate, 0);
    }  
}
