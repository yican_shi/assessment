package demo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class FxRates
{
    private HashMap<String,Double> map = null;
    private String csvFile;
    
    public static void main (String[] args)
    {
       
    }
    
    public void csv2Dict (String csvPath) {
        BufferedReader br = null;
        
        try {
            csvFile = csvPath;
            br = new BufferedReader(new FileReader(csvFile));
            String line =  null;
            map = new HashMap<String, Double>();
            
            while((line=br.readLine())!=null){
                String str[] = line.split(",");
                for(int i=1;i<str.length;i++){
                    map.put(str[0]+ "-" + str[1], Double.parseDouble (str[2]));
                }
            }

        }catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }  
    }
    
    public double getFXQoute (String country1, String country2) {
        String pair = country1 + "-" + country2;
        return map.get(pair); 
    }

}
